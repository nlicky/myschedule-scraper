# This file is part of myschedule-scraper.
#
# myschedule-scraper is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# myschedule-scraper is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with myschedule-scraper. If not, see <https://www.gnu.org/licenses/>.

from scraper_common import __version__
from scraper_common.scraper_common import ScraperCommon

import icalendar
import keyring

import argparse
import datetime
import getpass
import uuid


def date_input(input_msg):
    if '-' in input_msg:
        isodate = datetime.date.fromisoformat(input_msg)
        return isodate - datetime.timedelta(days=isodate.isoweekday() % 7)
    else:
        date_today = datetime.date.today()
        if input_msg.lower() == 'current_week':
            return date_today - datetime.timedelta(days=date_today.isoweekday() % 7)
        if input_msg.lower() == 'next_week':
            return date_today - datetime.timedelta(days=date_today.isoweekday() % 7) + datetime.timedelta(days=7)


parser = argparse.ArgumentParser(prog='myschedule-scraper_ics',
                                 description='Scrape from mySchedule and write events to an iCalendar file.',
                                 epilog='Arguments marked with [*] can accept current_week or next_week as the entered argument to start or stop scraping from the respective week.')
group = parser.add_mutually_exclusive_group()
group.add_argument('-p', '--password', default='', help='Password to log in')
group.add_argument('-m', '--manual_password', action='store_true', help='Input password manually')
group.add_argument('-k', '--keyring_python', help='Service name stored in Python keyring module to get a password (Usernames in argument and keyring must match)')
parser.add_argument('--show_useragent', action='store_true', help='Show the user agent used for scraping')
parser.add_argument('--useragent', help='Custom user agent to use when scraping. The user agent string must be encapsulated in quotation marks. If not used, a user agent chosen at random will be provided.')
parser.add_argument('--version', action='version', version='%(prog)s {}'.format(__version__))
parser.add_argument('user', help='Username to log in')
parser.add_argument('tz_timezone', help='Timezone name (TZ database name format)')
parser.add_argument('start_date', help='Start date to begin scraping from (YYYY-MM-DD format). [*]')
parser.add_argument('end_date', nargs='?', help='End date to stop scraping from (YYYY-MM-DD format). If not used, only the week specified in start_date will be scraped. [*]')

args = parser.parse_args()

if args.end_date is None:
    e_date = args.start_date
else:
    e_date = args.end_date

if args.keyring_python is not None:
    scrape_password = keyring.get_password(args.keyring_python, args.user)
elif args.manual_password is True:
    scrape_password = getpass.getpass()
else:
    scrape_password = args.password

filename = 'myschedule-{}.ics'.format(args.user)

try:
    f = open(filename, encoding='utf-8')
except FileNotFoundError:
    print('Could not find {}, a new file will be created'.format(filename))
    cal = icalendar.Calendar()
    cal.add('prodid', '-//myschedule-scraper//')
    cal.add('version', '2.0')
else:
    print('Found {}, file will be updated'.format(filename))
    cal = icalendar.Calendar.from_ical(f.read())
    f.close()

organizer = icalendar.vCalAddress('MAILTO:noone@example.com')
organizer.params['cn'] = icalendar.vText(args.user)
organizer.params['role'] = icalendar.vText('CHAIR')

attendee = icalendar.vCalAddress('MAILTO:{}@example.com'.format(args.user))
attendee.params['cn'] = icalendar.vText(args.user)
attendee.params['ROLE'] = icalendar.vText('REQ-PARTICIPANT')


class ScraperICS(ScraperCommon):
    def output(self, t_start, t_end, msg, place):
        event = icalendar.Event()

        event['organizer'] = organizer
        event['location'] = icalendar.vText(place)
        event.add('attendee', attendee, encode=0)
        event.add('priority', 5)

        event.add('summary', msg)
        event.add('dtstart', t_start)
        event.add('dtend', t_end)
        event.add('dtstamp', datetime.datetime.utcnow())

        event['uid'] = str(event['dtstamp'].to_ical().decode() + '-'
                           + str(uuid.uuid4())
                           + '@myschedule-scraper')

        cal.add_component(event)


scrape_ics = ScraperICS(date_input(args.start_date),
                        date_input(e_date),
                        args.tz_timezone, args.show_useragent, args.useragent)

scrape_ics.get_login(args.user, scrape_password)
scrape_ics.run()

f = open(filename, 'wb')
f.write(cal.to_ical())
f.close()
print('File written to {}'.format(filename))
