# This file is part of myschedule-scraper.
#
# myschedule-scraper is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# myschedule-scraper is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with myschedule-scraper. If not, see <https://www.gnu.org/licenses/>.

from bs4 import BeautifulSoup
from fake_useragent import UserAgent
import pytz
import requests

import datetime
import re
import sys
import time


class ScraperCommon:
    def __init__(self, pp_date, e_date, tzone, show_u_agent=None, u_agent=None):
        self.post_param_date = pp_date
        self.end_date = e_date
        self.pytz_timezone = pytz.timezone(tzone)

        self.website_str = ('https://myschedule.safeway.com/ESS/',
                            'AuthN/SwyLogin.aspx?ReturnUrl=%2fESS',
                            'Schedule.aspx')
        self.params_login = {'__VIEWSTATE': '',
                             '__VIEWSTATEGENERATOR': '',
                             'btnLogIn': 'Login',
                             'EmpID': '',
                             'Password': ''}
        # __VIEWSTATEGENERATOR is fetched once
        self.params_common = {'phTree': 'ctl00_tpTransfer_phTree',
                              'Othervalue': '[Other...]',
                              'tempItemPlaceHolder': 'ctl00_tpTransfer_tempPlaceHolder',
                              'tempSelectedChangeId': 'ctl00_tpTransfer_tempSeletedChangeId',
                              'overlayPanel': 'ctl00_tpTransfer_divTree',
                              'ctl00_tabContainer_ClientState': '{"ActiveTabIndex":0,"TabState":[true,true,true]}',
                              'ctl00$hdnActiveTab': '',
                              'ctl00$masterPlaceHolder$ddlDatePeriod': 'SPECIFIC_DATE',
                              '__EVENTARGUMENT': '',
                              '__LASTFOCUS': '',
                              'ActiveTab': 'ctl00_hdnActiveTab',
                              '__ASYNCPOST': 'true',
                              '__VIEWSTATEGENERATOR': ''}
        self.params_cal_init = {'ctl00$Master_ScriptManager': 'ctl00$masterPlaceHolder$UpdatePanel1|ctl00$masterPlaceHolder$ddlDatePeriod',
                                '__EVENTTARGET': 'ctl00$masterPlaceHolder$ddlDatePeriod',
                                '__VIEWSTATE': ''}
        # Strings that change each POST:
        # ctl00$masterPlaceHolder$txtWeekPeriodDate
        # __VIEWSTATE
        self.params_date = {'ctl00$Master_ScriptManager': 'ctl00$masterPlaceHolder$UpdatePanel1|ctl00$masterPlaceHolder$txtWeekPeriodDate',
                            '__EVENTTARGET': 'ctl00$masterPlaceHolder$txtWeekPeriodDate',
                            '__AjaxControlToolkitCalendarCssLoaded': '',
                            'ctl00$masterPlaceHolder$txtWeekPeriodDate': '',
                            '__VIEWSTATE': ''}

        self.show_user_agent = show_u_agent

        if u_agent is not None:
            self.user_agent = u_agent
        else:
            self.user_agent = UserAgent().random

        self.strptime_format = '%I:%M%p %Y%m/%d'
        self.time_off = ('Unavail', 'Unpaid', 'Vac', 'Float', 'Leave',
                         'Anniversary', 'Birthday')

    def get_login(self, usr_name, usr_pass):
        self.params_login['EmpID'] = usr_name
        self.params_login['Password'] = usr_pass

    def parse(self, text_html):
        soup = BeautifulSoup(text_html, 'html.parser')
        datelist = soup.find_all('div', 'date')

        def strptime_str(time_str):
            year_fix = self.post_param_date.year

            if int(month_day_re[:2]) < int(self.post_param_date.month):
                year_fix += 1

            return str(time_str
                       + 'm '
                       + str(year_fix)
                       + month_day_re)

        for eventdate in datelist:
            if any(toff_str in str(eventdate.parent) for toff_str in self.time_off):
                continue

            if 'hours' in str(eventdate.parent):
                month_day_re = re.search(r'(\d{2}/\d{2})', str(eventdate.parent)).group(1)
                time_re = re.search(r'(\d{1,2}:\d{2}[ap]) - (\d{1,2}:\d{2}[ap])', str(eventdate.parent))
                page_hour = re.search(r'Hours:\s(\d{1,2}\.\d{2})\D', str(eventdate.parent)).group(1)
                location_re = re.search(r'(Store:\s\d+)\D', str(eventdate.parent)).group(1)

                time_start = self.pytz_timezone.localize(
                             datetime.datetime.strptime(strptime_str(time_re.group(1)), self.strptime_format))
                time_end = self.pytz_timezone.localize(
                           datetime.datetime.strptime(strptime_str(time_re.group(2)), self.strptime_format))

                if time_end < time_start:
                    time_end += datetime.timedelta(days=1)
                    time_end = self.pytz_timezone.normalize(time_end)

                event_str = page_hour + 'h'
                if float(page_hour) * 3600 < (time_end - time_start).total_seconds():
                    hour_diff = ((time_end - time_start).total_seconds() / 3600) - float(page_hour)
                    event_str += (' +'
                                  + '{:.2f}'.format(hour_diff)
                                  + 'h Break')

                self.output(time_start, time_end, event_str, location_re)

    def output(self, t_start, t_end, msg, place):
        print('Start date: ' + str(t_start))
        print('End date: ' + str(t_end))
        print('Event message: ' + str(msg))
        print('Location: ' + str(place))

    def run(self):
        with requests.Session() as s:
            s.headers.update({'User-Agent': str(self.user_agent)})
            if self.show_user_agent:
                print(str(self.user_agent))

            g = s.get(self.website_str[0] + self.website_str[1])
            soup = BeautifulSoup(g.text, 'html.parser')
            self.params_login['__VIEWSTATE'] = soup.find(id='__VIEWSTATE')['value']
            self.params_login['__VIEWSTATEGENERATOR'] = soup.find(id='__VIEWSTATEGENERATOR')['value']

            p = s.post(self.website_str[0] + self.website_str[1],
                       data=self.params_login)
            g = s.get(self.website_str[0] + self.website_str[2])

            if 'mySchedule' in g.text:
                print('Logged in as {}'.format(self.params_login['EmpID']))
            else:
                sys.exit('Login failed, check your login name and/or password')

            soup = BeautifulSoup(g.text, 'html.parser')
            self.params_cal_init['__VIEWSTATE'] = soup.find(id='__VIEWSTATE')['value']
            self.params_common['__VIEWSTATEGENERATOR'] = soup.find(id='__VIEWSTATEGENERATOR')['value']
            self.params_cal_init.update(self.params_common)
            self.params_date.update(self.params_common)

            p = s.post(self.website_str[0] + self.website_str[2],
                       data=self.params_cal_init)
            self.params_date['__VIEWSTATE'] = re.search(r'__VIEWSTATE\|(.+?)\|', p.text).group(1)

            while self.post_param_date <= self.end_date:
                time.sleep(0.25)
                self.params_date['ctl00$masterPlaceHolder$txtWeekPeriodDate'] = self.post_param_date.strftime('%m/%d/%Y')
                p = s.post(self.website_str[0] + self.website_str[2],
                           data=self.params_date)
                self.params_date['__VIEWSTATE'] = re.search(r'__VIEWSTATE\|(.+?)\|', p.text).group(1)

                self.parse(p.text)

                print(self.post_param_date.strftime('Parsed week of %x'))
                self.post_param_date += datetime.timedelta(days=7)
