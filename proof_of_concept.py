# This file is part of myschedule-scraper.
#
# myschedule-scraper is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# myschedule-scraper is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with myschedule-scraper. If not, see <https://www.gnu.org/licenses/>.

from scraper_common.scraper_common import ScraperCommon

import datetime
import getpass


def date_input(input_msg):
    while True:
        try:
            isodate = datetime.date.fromisoformat(input(input_msg))
            break
        except ValueError:
            print('Please enter a date in YYYY-MM-DD format')
    return isodate - datetime.timedelta(days=isodate.isoweekday() % 7)


print('mySchedule Scraper')
interact = ScraperCommon(date_input('Start date (YYYY-MM-DD): '),
                         date_input('End date (YYYY-MM-DD): '),
                         input('Timezone (TZ database name): '))

interact.get_login(input('User: '), getpass.getpass())
interact.run()
