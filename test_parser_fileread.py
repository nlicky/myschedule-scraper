# This file is part of myschedule-scraper.
#
# myschedule-scraper is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# myschedule-scraper is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with myschedule-scraper. If not, see <https://www.gnu.org/licenses/>.

import datetime
import sys
from scraper_common.scraper_common import ScraperCommon


try:
    filename = sys.argv[1]
    test_date = sys.argv[2]
    test_timezone = sys.argv[3]
except IndexError:
    print('Enter a file path argument for test input, followed by a date in YYYY-MM-DD format, and a timezone in TZ database name format.')
    sys.exit()

f = open(filename)
test_scraper_common = ScraperCommon(datetime.date.fromisoformat(test_date), '', test_timezone)
test_scraper_common.parse(f)
f.close()
