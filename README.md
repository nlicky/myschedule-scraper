# myschedule-scraper
A scraper for the mySchedule website that converts schedule events to iCalendar format

# Requirements
* Python 3.7+
* [beautifulsoup4](https://crummy.com/software/BeautifulSoup/)
* [fake_useragent](https://github.com/hellysmile/fake-useragent)
* [icalendar](https://github.com/collective/icalendar)
* [keyring](https://github.com/jaraco/keyring)
* [pytz](https://launchpad.net/pytz)
* [requests](https://github.com/requests/requests)

# Usage
## scraper_ics
Scrape the mySchedule website from a certain date range using login credentials and create an iCalendar file. The scraper will automatically update an existing file if one exists. [A list of TZ database timezones can be found on Wikipedia.](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones)

To easily enter the current week or next week as a date, use `current_week` or `next_week` as the argument for either `start_date` or `end_date`.

Note that `end_date` is an optional argument. If `end_date` is not used, only the week specified in `start_date` will be scraped.

`python3.7 scraper_ics.py user tz_timezone start_date [end_date] -p PASSWORD`

A better option is to enter the password manually upon starting the scraper. This ensures that the password is not stored in the command history.

`python3.7 scraper_ics.py user tz_timezone start_date [end_date] -m`

Alternatively, if there are credentials stored using the Python keyring module, that service can also be used instead of putting the password in via plaintext. Note that the usernames in both the argument and keyring must match for this to work.

`python3.7 scraper_ics.py user tz_timezone start_date [end_date] -k KEYRING_PYTHON`

## proof_of_concept
Use the scraper interactively, where each argument is entered one at a time. Scraper output is sent directly to the terminal, with no file output.

This Python script is mainly used for code testing, but it can also serve as an introduction on how to use the scraper.

## test_parser_fileread
Test the parser by reading a text dump of the website's XHR response along with a supplied date and a timezone name from the TZ database. Output is sent directly to the terminal.

`python3.7 test_parser_fileread.py path/to/file start_date tz_timezone`

# Building
[PyInstaller](https://github.com/pyinstaller/pyinstaller/) is the preferred freezing tool to build binaries. Just run PyInstaller on a script to freeze it into an executable.

`pyinstaller script.py --onefile`

Don't forget when running a Windows binary to use `script.exe` rather than running the script with Python.

# License
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/gpl-3.0.txt>.
