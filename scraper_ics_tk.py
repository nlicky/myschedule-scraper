# This file is part of myschedule-scraper.
#
# myschedule-scraper is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# myschedule-scraper is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with myschedule-scraper. If not, see <https://www.gnu.org/licenses/>.

from scraper_common import __version__

import pytz

from tkinter import *
from tkinter.ttk import *

import os
import subprocess
import sys
from threading import Thread
import webbrowser


class Application(Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master

        self.SCRAPER_ICS_BIN = 'scraper_ics'
        if os.name == 'nt':
            self.SCRAPER_ICS_BIN += '.exe'

        if getattr(sys, 'frozen', False):
            application_path = sys._MEIPASS
        else:
            application_path = os.path.dirname(os.path.abspath(__file__))

        self.WEBSITE_URL = 'https://gitlab.com/nlicky/myschedule-scraper'
        self.LICENSE = os.path.join(application_path, 'LICENSE')

        self.user_str = StringVar()
        self.password_str = StringVar()
        self.timezone_str = StringVar()
        self.user_agent_str = StringVar()
        self.start_date_str = StringVar()

        self.scraper_ics_subprocess = None
        self.error_window = None
        self.about_window = None

        self.error_text = None
        self.open_error_window()
        self.error_window.withdraw()

        self.master.title('scraper_ics_tk')
        self.master.option_add('*tearOff', False)
        self.create_widgets()

    def create_widgets(self):
        password_bool = BooleanVar()
        user_agent_bool = BooleanVar()
        show_user_agent_bool = BooleanVar()
        end_date_bool = BooleanVar()
        end_date_str = StringVar()

        def toggle_password():
            if password_bool.get():
                password_entry.config(show='')
            else:
                password_entry.config(show='•')

        def toggle_user_agent():
            if user_agent_bool.get():
                user_agent_entry.config(state='enable')
            else:
                user_agent_entry.config(state='disable')

        def toggle_end_date():
            for widget in end_date_lf.winfo_children():
                if end_date_bool.get():
                    widget.config(state='enable')
                else:
                    widget.config(state='disable')

        def toggle_subprocess_buttons():
            if str(subprocess_stop_button['state']) == 'disabled':
                subprocess_button.config(state='disabled')
                subprocess_stop_button.config(state='normal')
            else:
                subprocess_button.config(state='normal')
                subprocess_stop_button.config(state='disabled')

        def get_week(date_str, date_entry):
            if date_str.get() == 'range':
                return date_entry.get()
            else:
                return date_str.get()

        def subprocess_scraper_ics():
            s_date = get_week(self.start_date_str, start_date_entry)

            scraper_ics_command = [self.SCRAPER_ICS_BIN, self.user_str.get(),
                                   '-p' + self.password_str.get(),
                                   self.timezone_str.get(),
                                   s_date]

            if end_date_bool.get():
                scraper_ics_command.append(get_week(end_date_str,
                                                    end_date_entry))
            if show_user_agent_bool.get():
                scraper_ics_command.append('--show_useragent')
            if user_agent_bool.get():
                scraper_ics_command.extend(('--useragent',
                                            self.user_agent_str.get()))

            subprocess_text.config(state='normal')
            subprocess_text.delete('1.0', END)
            subprocess_text.config(state='disabled')

            toggle_subprocess_buttons()

            self.scraper_ics_subprocess = subprocess.Popen(scraper_ics_command,
                                                           stdout=
                                                           subprocess.PIPE,
                                                           stderr=
                                                           subprocess.PIPE)
            text_thread = Thread(target=self.read_output,
                                 args=(self.scraper_ics_subprocess.stdout,
                                       subprocess_text,
                                       toggle_subprocess_buttons))
            error_thread = Thread(target=self.read_output,
                                 args=(self.scraper_ics_subprocess.stderr,
                                       self.error_text))
            text_thread.start()
            error_thread.start()

        def stop_subprocess():
            if self.scraper_ics_subprocess.poll() is None:
                self.scraper_ics_subprocess.kill()
                self.update_text('Stopped scraper process', subprocess_text)

        def bind_return_subprocess():
            try:
                if self.scraper_ics_subprocess.poll() is not None:
                    subprocess_scraper_ics()
            except AttributeError:
                subprocess_scraper_ics()

        data_frame = Frame(self.master)

        login_lf = Labelframe(data_frame, text='Login')
        user_label = Label(login_lf, text='User:')
        user_entry = Entry(login_lf, textvariable=self.user_str)
        password_label = Label(login_lf, text='Password:')
        password_entry = Entry(login_lf, show='•',
                               textvariable=self.password_str)
        password_show = Checkbutton(login_lf, text='Show password',
                                    command=toggle_password,
                                    variable=password_bool,
                                    onvalue=True, offvalue=False)

        options_lf = Labelframe(data_frame, text='Options')
        timezone_label = Label(options_lf, text='Timezone:')
        timezone_combobox = Combobox(options_lf, values=pytz.common_timezones,
                                     textvariable=self.timezone_str)
        end_date_enable = Checkbutton(options_lf, text='Enable End Date',
                                      command=toggle_end_date,
                                      variable=end_date_bool,
                                      onvalue=True, offvalue=False)
        user_agent_enable = Checkbutton(options_lf, text='User agent:',
                                       command=toggle_user_agent,
                                       variable=user_agent_bool, onvalue=True,
                                       offvalue=False)
        user_agent_entry = Entry(options_lf, textvariable=self.user_agent_str)
        show_user_agent = Checkbutton(options_lf,
                                      text='Show user agent in output',
                                      variable=show_user_agent_bool,
                                      onvalue=True, offvalue=False)

        start_date_lf = Labelframe(data_frame, text='Start Date')
        start_date_current = Radiobutton(start_date_lf, text='Current Week',
                                         variable=self.start_date_str,
                                         value='current_week')
        start_date_next = Radiobutton(start_date_lf, text='Next Week',
                                      variable=self.start_date_str,
                                      value='next_week')
        start_date_range = Radiobutton(start_date_lf,
                                       text='Date Range:\n(YYYY-MM-DD)',
                                       variable=self.start_date_str,
                                       value='range')
        start_date_entry = Entry(start_date_lf, width=10)

        end_date_lf = Labelframe(data_frame, text='End Date')
        end_date_current = Radiobutton(end_date_lf, text='Current Week',
                                       variable=end_date_str, value='current_week')
        end_date_next = Radiobutton(end_date_lf, text='Next Week',
                                    variable=end_date_str, value='next_week')
        end_date_range = Radiobutton(end_date_lf,
                                     text='Date Range:\n(YYYY-MM-DD)',
                                     variable=end_date_str, value='range')
        end_date_entry = Entry(end_date_lf, width=10)


        controls_frame = Frame(data_frame)
        subprocess_button = Button(controls_frame, text='Run',
                                   command=subprocess_scraper_ics)
        subprocess_stop_button = Button(controls_frame, text='Stop',
                                        command=stop_subprocess,
                                        state='disabled')
        self.master.bind('<Return>', lambda event: bind_return_subprocess())

        subprocess_frame = Frame(self.master)
        subprocess_text = Text(subprocess_frame, width=80, height=10,
                               wrap='word', state='disabled')
        subprocess_scroll = Scrollbar(subprocess_frame,
                                      command=subprocess_text.yview)
        subprocess_text.config(yscrollcommand=subprocess_scroll.set)

        data_frame.grid(column=0, row=0, sticky='ew')

        login_lf.grid(column=0, row=0)
        user_label.grid(column=0, row=0)
        user_entry.grid(column=1, row=0)
        password_label.grid(column=0, row=1)
        password_entry.grid(column=1, row=1)
        password_show.grid(column=0, row=2)

        options_lf.grid(column=1, row=0)
        timezone_label.grid(column=0, row=0)
        timezone_combobox.grid(column=1, row=0)
        end_date_enable.grid(column=0, row=1)
        user_agent_enable.grid(column=0, row=2)
        user_agent_entry.grid(column=1, row=2)
        show_user_agent.grid(column=0, row=3)

        start_date_lf.grid(column=2, row=0)
        start_date_current.grid(column=0, row=1)
        start_date_next.grid(column=0, row=2)
        start_date_range.grid(column=0, row=3)
        start_date_entry.grid(column=1, row=3)

        end_date_lf.grid(column=3, row=0)
        end_date_current.grid(column=0, row=1)
        end_date_next.grid(column=0, row=2)
        end_date_range.grid(column=0, row=3)
        end_date_entry.grid(column=1, row=3)

        controls_frame.grid(column=4, row=0)
        subprocess_button.grid(column=0, row=0)
        subprocess_stop_button.grid(column=0, row=1)

        subprocess_frame.grid(column=0, row=1, sticky='nsew')
        subprocess_text.grid(column=0, row=0, sticky='nsew')
        subprocess_scroll.grid(column=1, row=0, sticky='nsw')

        self.master.columnconfigure(0, weight=1)
        self.master.rowconfigure(1, weight=1)
        subprocess_frame.columnconfigure(0, weight=1)
        subprocess_frame.rowconfigure(0, weight=1)

        menubar = Menu(self.master)
        self.master.config(menu=menubar)

        menu_file = Menu(menubar)
        menubar.add_cascade(menu=menu_file, label='File')
        menu_file.add_command(label='Exit', command=self.master.quit)

        menu_help = Menu(menubar)
        menubar.add_cascade(menu=menu_help, label='Help')
        menu_help.add_command(label='Error Log',
                              command=self.error_window.deiconify)
        menu_help.add_command(label='About scraper_ics_tk',
                              command=lambda:
                              self.open_window(self.about_window,
                                               self.open_about_window))

        for frame in data_frame.winfo_children():
            frame.grid_configure(padx=5, pady=5, sticky='n')

            for widget in frame.winfo_children():
                widget.grid_configure(padx=2, pady=2, sticky='w')

        for widget in end_date_lf.winfo_children():
            widget.config(state='disable')

        user_agent_entry.config(state='disable')

    def update_text(self, output, textbox):
        if textbox['state'] == 'disabled':
            textbox.config(state='normal')

        textbox.insert(END, output)
        textbox.config(state='disabled')
        textbox.see(END)

    def read_output(self, output, textbox, toggle_buttons=None):
        for line in iter(output.readline, b''):
            self.update_text(line, textbox)
        output.close()

        if toggle_buttons is not None:
            toggle_buttons()

    def open_window(self, window_state, open_child):
        try:
            if not window_state.winfo_exists():
                open_child()
        except AttributeError:
            open_child()

    def open_error_window(self):
        self.error_window = Toplevel(self.master)
        self.error_window.title('Error Log')
        self.error_window.protocol('WM_DELETE_WINDOW',
                                   self.error_window.withdraw)

        error_frame = Frame(self.error_window)
        self.error_text = Text(error_frame, width=40, height=20,
                          wrap='word', state='disabled')
        error_scroll = Scrollbar(error_frame,
                                 command=self.error_text.yview)
        self.error_text.config(yscrollcommand=error_scroll.set)

        error_frame.grid(column=0, row=0, sticky='nsew')
        self.error_text.grid(column=0, row=0, sticky='nsew')
        error_scroll.grid(column=1, row=0, sticky='nsw')

        self.error_window.columnconfigure(0, weight=1)
        self.error_window.rowconfigure(0, weight=1)
        error_frame.columnconfigure(0, weight=1)
        error_frame.rowconfigure(0, weight=1)

    def open_about_window(self):
        scraper_ics_command = (self.SCRAPER_ICS_BIN, '--version')

        self.about_window = Toplevel(self.master)
        self.about_window.title('About scraper_ics_tk')
        self.about_window.resizable(False, False)

        about_notebook = Notebook(self.about_window)

        info_frame = Frame(about_notebook)
        about_notebook.add(info_frame, text='Info')
        info_center_frame = Frame(info_frame)
        name_label = Label(info_center_frame, text='scraper_ics_tk',
                           font='TkCaptionFont')
        caption_label = Label(info_center_frame,
                              text='GUI interface for scraper_ics')
        website_button = Button(info_center_frame, text='Website',
                                command=lambda:
                                        webbrowser.open(self.WEBSITE_URL))
        version_text = Text(info_center_frame, width=30, height=4,
                            state='disabled')
        about_close_button = Button(self.about_window, text='Close',
                                    command=self.about_window.destroy)

        license_frame = Frame(about_notebook)
        about_notebook.add(license_frame, text='License')
        license_text = Text(license_frame, width=60, height=30, wrap='word',
                            state='disabled')
        license_scroll = Scrollbar(license_frame, command=license_text.yview)
        license_text.config(yscrollcommand=license_scroll.set)

        about_notebook.grid(column=0, row=0)
        about_close_button.grid(column=0, row=1, sticky='e')
        about_close_button.grid_configure(padx=5, pady=5)

        info_frame.columnconfigure(0, weight=1)
        info_frame.rowconfigure(0, weight=1)

        info_center_frame.grid(column=0, row=0)
        name_label.grid(column=0, row=0)
        caption_label.grid(column=0, row=1)
        website_button.grid(column=0, row=2)
        version_text.grid(column=0, row=3)

        license_text.grid(column=0, row=0, sticky='nsew')
        license_scroll.grid(column=1, row=0, sticky='nsw')

        for widget in info_center_frame.winfo_children():
            widget.grid_configure(padx=5, pady=5)

        try:
            f = open(self.LICENSE)
        except FileNotFoundError as e:
            self.update_text(e, license_text)
        else:
            license_text.config(state='normal')
            license_text.insert(END, f.read())
            license_text.config(state='disabled')
            f.close()

        self.update_text('Tk Version {}'.format(TkVersion)
                         + '\n'
                         + 'scraper_ics_tk Version {}'.format(__version__)
                         + '\n',
                         version_text)

        scraper_ics_version = subprocess.Popen(scraper_ics_command,
                                               stdout=subprocess.PIPE)
        text_thread = Thread(target=self.read_output,
                             args=(scraper_ics_version.stdout, version_text))
        text_thread.start()

root = Tk()
app = Application(master=root)
app.mainloop()
